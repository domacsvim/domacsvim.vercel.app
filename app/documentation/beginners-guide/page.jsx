import { Codeblock } from "../../../components/code"

const page = () => {
  return (
    <div className="flex flex-col w-full h-fit">
      <h1 id="beginners-guide">Beginner&apos;s Guide</h1>
      <p className="mb-5">
        The foundation and structure of DomacsVim is based on Vim and NeoVim.
        So, for ease of work, it&apos;s better to have a basic knowledge of
        modes, default editor commands, globals, mappings, etc.
      </p>
      <p className="mb-5">
        To get an overview of Vim, you can run the following command in your
        terminal:
      </p>
      <Codeblock language="bash" showLineNumbers={false}>
        $ vimtutor
      </Codeblock>
      <p className="my-5">
        To get an overview of Neovim, you can run the following command inside
        Neovim:
      </p>
      <Codeblock language="bash" showLineNumbers={false}>
        :Tutor
      </Codeblock>
      <h2 id="learn-lua-lang">Learn Lua Language</h2>
      <p className="my-5">
        Domacsvim is written with lua programming language and you definitely
        need to know the basics of lua language to configure or cooperate in it
        (it&apos;s not difficult).
      </p>
      <Codeblock language="bash" showLineNumbers={false}>
        :help lua-guide
      </Codeblock>
      <p className="my-5">
        Or check the{" "}
        <a href="https://neovim.io/doc/user/lua-guide.html#lua-guide">
          official documentation
        </a>
      </p>
    </div>
  )
}

export default page
