import { Copycode, Codetag, Codeblock } from "../../../../components/code"

const page = () => {
  return (
    <div className="flex flex-col w-full h-fit">
      <h1 id="configuration">configuration</h1>
      <p className="mb-5">
        Configuration file located in{" "}
        <Codetag language="bash">~/.config/dvim/init.lua</Codetag>
      </p>
      <p className="mb-5">
        By default, this file is created in the first run of the program. And
        there is such a content in it:
      </p>
      <div className="flex w-full">
        <Copycode language="lua" showLineNumbers={true} wrap={false}>
          {`-- add custom keys
dvim.keys = {}
-- add custom plugins
dvim.plugins = {}
-- set leaderkey
dvim.mapleader = " "
-- set theme
dvim.colorscheme = "onedark"
-- change configs
-- you can see the modules in here :
--   for module, _ in pairs(dvim.core) do
--     print(module)
--   end
-- template :
-- dvim.core.[module].[pattern].[...]
-- example :
dvim.core.nvimtree.view.width = 31
-- change default icons
-- dvim.icons.[category].[icon]
-- categorys :
--    devicons, ui, kind_icons
-- dvim.icons.ui.search = " "
-- dvim.icons.devicons["html"] = " "
-- dvim.icons.kind_icons.folder = " "
dvim.icons.kind_icons.folder = " "
-- change options
-- you can see the options in here :
--   vim.opt.backup = false, -- creates a backup file
--   vim.opt.clipboard = "unnamedplus", -- allows neovim to access the system clipboard
--   vim.opt.cmdheight = 1, -- more space in the neovim command line for displaying messages
--   vim.opt.completeopt = "menu,menuone,noselect",
--   vim.opt.conceallevel = 0, -- so that \`\` is visible in markdown files
--   vim.opt.fileencoding = "utf-8", -- the encoding written to a file
--   vim.opt.foldmethod = "manual", -- folding, set to "expr" for treesitter based folding
--   vim.opt.foldexpr = "", -- set to "nvim_treesitter#foldexpr()" for treesitter based folding
--   vim.opt.guifont = "monospace:h17", -- the font used in graphical neovim applications
--   vim.opt.hidden = true, -- required to keep multiple buffers and open multiple buffers
--   vim.opt.hlsearch = true, -- highlight all matches on previous search pattern
--   vim.opt.ignorecase = true, -- ignore case in search patterns
--   vim.opt.mouse = "a", -- allow the mouse to be used in neovim
--   vim.opt.pumheight = 10, -- pop up menu height
--   vim.opt.showmode = false, -- we don't need to see things like -- INSERT -- anymore
--   vim.opt.smartcase = true, -- smart case
--   vim.opt.splitbelow = true, -- force all horizontal splits to go below current window
--   vim.opt.splitright = true, -- force all vertical splits to go to the right of current window
--   vim.opt.swapfile = false, -- creates a swapfile
--   vim.opt.termguicolors = true, -- set term gui colors (most terminals support this)
--   vim.opt.timeoutlen = 1000, -- time to wait for a mapped sequence to complete (in milliseconds)
--   vim.opt.title = true, -- set the title of window to the value of the titlestring
--   vim.opt.titlestring = "%<%F - DomacsVim", -- what the title of the window will be set to
--   vim.opt.undodir = undodir, -- set an undo directory
--   vim.opt.undofile = true, -- enable persistent undo
--   vim.opt.updatetime = 100, -- faster completion
--   vim.opt.writebackup = false, -- if a file is being edited by another program
--   vim.opt.expandtab = true, -- convert tabs to spaces
--   vim.opt.shiftwidth = 2, -- the number of spaces inserted for each indentation
--   vim.opt.tabstop = 2, -- insert 2 spaces for a tab
--   vim.opt.cursorline = true, -- highlight the current line
--   vim.opt.number = true, -- set numbered lines
--   vim.opt.numberwidth = 4, -- set number column width to 2 {default 4}
--   vim.opt.signcolumn = "yes", -- always show the sign column, otherwise it would shift the text each time
--   vim.opt.wrap = false, -- display lines as one long line
--   vim.opt.shadafile = dvim_cache_dir .. "/dvim.shada",
--   vim.opt.scrolloff = 8, -- minimal number of screen lines to keep above and below the cursor.
--   vim.opt.sidescrolloff = 8, -- minimal number of screen lines to keep left and right of the cursor.
--   vim.opt.showcmd = false,
--   vim.opt.ruler = false,
--   vim.opt.laststatus = 3,
-- Lspconfig:
-- vim.lsp.set_log_level("debug")
-- dvim.core.lspconfig.[pattern]

-- you can add you own structure :
-- for example, add lua/ directory and and write plugins.lua in here or add lua/configs or lua/keymappings or ...`}
        </Copycode>
      </div>
      <h2 id="keybindings">Keybindings</h2>
      <p className="mb-5">
        The default leader key is : <Codetag language="text">Space</Codetag>,
        you can chane it with :
      </p>
      <Codeblock language="lua">dvim.mapleader = &quot &quot</Codeblock>
      <p className="my-5">To set the new key in the desired mode, you can:</p>
      <Codeblock language="lua">
        {`dvim.keys = \{
  command_mode = \{\},
  normal_mode = \{\},
  term_mode = \{\},
  visual_mode = \{\},
\}`}
      </Codeblock>
      <h3 id="mapping-format">Mapping format</h3>
      <Codeblock language="lua">
        {`dvim.keys = \{
  normal_mode = {
    ["keys"] = "action",
    [";"] = ":",
    ["<leader>tt"] = function()
       return require("base46").toggle_transparency()
    end,
  }
\}`}
      </Codeblock>
      <h3 id="whichkey">Whichkey</h3>
      <Codeblock language="lua">
        {`dvim.core.whichkey.mappings\['key'\] = \{val\}`}
      </Codeblock>
      <h3 id="whichkey-format">Whichkey format</h3>
      <Codeblock language="lua">
        {`dvim.core.whichkey.mappings\['key'\] = "value"
dvim.core.whichkey.mappings\['key'\] = \{
  name = "",
  ['sub-key1'] = "value1",
  ['sub-key2'] = "value2",
  ...
\}`}
      </Codeblock>
      <h2 id="plugins">Plugins</h2>
      <p className="mb-5">
        The configurations for core (builtin) plugins are accessible through the{" "}
        <Codetag language="text">dvim.core</Codetag> table.
        <br />
        You can enable or disable core plugins with this syntax :
      </p>
      <Codeblock language="lua">
        {`dvim.core.alpha.active = false
dvim.core.nvimtree.active = false
dvim.core.bufferline.active = false`}
      </Codeblock>
      <p className="my-5">And to add your custom plugins : </p>
      <Codeblock language="lua">
        {`dvim.plugins = \{
  \{
    "Repository URL", -- for example: https://github.com/wfxr/minimap.vim
    enable = true, -- When false, or if the function returns false, then this plugin will not be included in the spec
    lazy = true, -- When true, the plugin will only be loaded when needed. Lazy-loaded plugins are automatically loaded when their Lua modules are required, or when one of the lazy-loading handlers triggers
    dependencies = {} -- A list of plugin names or plugin specs that should be loaded when the plugin loads. Dependencies are always lazy-loaded unless specified otherwise. When specifying a name, make sure the plugin spec has been defined somewhere else.
    opts = {} -- opts should be a table (will be merged with parent specs), return a table (replaces parent specs) or should change a table. The table will be passed to the Plugin.config() function. Setting this value will imply Plugin.config()
    config = function() end -- config is executed when the plugin loads. The default implementation will automatically run require(MAIN).setup(opts). Lazy uses several heuristics to determine the plugin's MAIN module automatically based on the plugin's name. See also opts. To use the default implementation without opts set config to true.
  \},
\}`}
      </Codeblock>
      <p className="mt-5">
        You can see another options in{" "}
        <a
          href="https://github.com/folke/lazy.nvim#-plugin-spec"
          target="_blank"
        >
          here
        </a>
      </p>
      <h3 id="configure-builtin-plugins">Configure builtin plugins</h3>
      <Codeblock language="lua">
        {`dvim.core.nvimtree.view.width = 61
dvim.core.gitsigns.signcolumn = false`}
      </Codeblock>
      <span className="my-5 py-3 px-2 bg-red-600 rounded-md border-l-red-900 border-l-8">
        <b>
          BUT : if you use this syntax, you will face problem because you will
          overwrite all the configurations in this table :{" "}
        </b>
      </span>
      <Codeblock language="lua">
        {`dvim.core.nvimtree = \{
  view = \{
    width = 61
  \}
  ...
\}`}
      </Codeblock>
      <h2 id="colorschemes">Colorschemes</h2>
      <Codeblock language="lua">{`dvim.colorscheme = 'theme'`}</Codeblock>
      <h2 id="default-icons">Default icons</h2>
      <Codeblock language="lua">
        {`-- dvim.icon.[category].[icon] = 'your icon'
-- categorys :
--    devicons, ui, kind_icons
dvim.icons.ui.search = "..."
dvim.icons.devicons["html"] = "..." -- or dvim.icons.devicons.html
dvim.icons.kind_icons.folder = "..."`}
      </Codeblock>
      <h2 id="options">Options</h2>
      <div className="flex w-full">
        <Codeblock language="lua">
          {`vim.opt.backup = false, -- creates a backup file
vim.opt.clipboard = "unnamedplus", -- allows neovim to access the system clipboard
vim.opt.cmdheight = 1, -- more space in the neovim command line for displaying messages
vim.opt.completeopt = "menu,menuone,noselect",
vim.opt.conceallevel = 0, -- so that \`\` is visible in markdown files
vim.opt.fileencoding = "utf-8", -- the encoding written to a file
vim.opt.foldmethod = "manual", -- folding, set to "expr" for treesitter based folding
vim.opt.foldexpr = "", -- set to "nvim_treesitter#foldexpr()" for treesitter based folding
vim.opt.guifont = "monospace:h17", -- the font used in graphical neovim applications
vim.opt.hidden = true, -- required to keep multiple buffers and open multiple buffers
vim.opt.hlsearch = true, -- highlight all matches on previous search pattern
vim.opt.ignorecase = true, -- ignore case in search patterns
vim.opt.mouse = "a", -- allow the mouse to be used in neovim
vim.opt.pumheight = 10, -- pop up menu height
vim.opt.showmode = false, -- we don't need to see things like -- INSERT -- anymore
vim.opt.smartcase = true, -- smart case
vim.opt.splitbelow = true, -- force all horizontal splits to go below current window
vim.opt.splitright = true, -- force all vertical splits to go to the right of current window
vim.opt.swapfile = false, -- creates a swapfile
vim.opt.termguicolors = true, -- set term gui colors (most terminals support this)
vim.opt.timeoutlen = 1000, -- time to wait for a mapped sequence to complete (in milliseconds)
vim.opt.title = true, -- set the title of window to the value of the titlestring
vim.opt.titlestring = "%<%F - DomacsVim", -- what the title of the window will be set to
vim.opt.undodir = undodir, -- set an undo directory
vim.opt.undofile = true, -- enable persistent undo
vim.opt.updatetime = 100, -- faster completion
vim.opt.writebackup = false, -- if a file is being edited by another program
vim.opt.expandtab = true, -- convert tabs to spaces
vim.opt.shiftwidth = 2, -- the number of spaces inserted for each indentation
vim.opt.tabstop = 2, -- insert 2 spaces for a tab
vim.opt.cursorline = true, -- highlight the current line
vim.opt.number = true, -- set numbered lines
vim.opt.numberwidth = 4, -- set number column width to 2 {default 4}
vim.opt.signcolumn = "yes", -- always show the sign column, otherwise it would shift the text each time
vim.opt.wrap = false, -- display lines as one long line
vim.opt.shadafile = dvim_cache_dir .. "/dvim.shada",
vim.opt.scrolloff = 8, -- minimal number of screen lines to keep above and below the cursor.
vim.opt.sidescrolloff = 8, -- minimal number of screen lines to keep left and right of the cursor.
vim.opt.showcmd = false,
vim.opt.ruler = false,
vim.opt.laststatus = 3`}
        </Codeblock>
      </div>
    </div>
  )
}

export default page
