import { Codeblock, Copycode } from "../../../components/code"

const page = () => {
  return (
    <div className="flex flex-col w-full">
      <h1 id="development">Development</h1>
      <p>
        why not ! We accept any contribution and help in different sectors and
        we do not consider any of them to be small or unimportant. The only way
        to participate in this project is not to write or change the code, but
        you can also raise your ideas and opinions about adding a new feature,
        logo, interior design and etc.
      </p>
      <h2 id="structure">Structure</h2>
      <Codeblock language="bash" showLineNumbers={false}>
        {`├── LICENSE
├── Makefile
├── README.md
├── bin
│   └── domacsvim-cli-template.sh
├── config.example.lua
├── database
│   └── *
├── init.lua
├── installer
│   └── install.sh
└── lua
    ├── bootstrap.lua
    ├── config
    │   ├── defaults.lua
    │   └── init.lua
    ├── core
    │   ├── autopairs.lua
    │   ├── bufferline.lua
    │   ├── cmp.lua
    │   ├── dap.lua
    │   ├── dashboard.lua
    │   ├── gitsigns.lua
    │   ├── init.lua
    │   ├── log.lua
    │   ├── mason.lua
    │   ├── nvimtree.lua
    │   ├── telescope.lua
    │   ├── terminal.lua
    │   ├── treesitter.lua
    │   └── whichkey.lua
    ├── help
    │   └── init.lua
    ├── icons.lua
    ├── keymappings
    │   ├── keymappings.lua
    │   └── manager.lua
    ├── lsp
    │   ├── defaults.lua
    │   ├── init.lua
    │   ├── luasnip.lua
    │   └── manager.lua
    ├── options.lua
    ├── plugins
    │   ├── manager.lua
    │   └── plugins.lua
    └── utils.lua

10 directories, 36 files`}
      </Codeblock>
      <h2 id="get-started">Get started</h2>
      <p className="mb-5">
        To getting started, it&apos;s better to create a symbolic link of the
        project :
      </p>
      <Copycode language="bash">
        ln -s ~/.local/share/domacsvim ~/projects/domacsvim
      </Copycode>
      <p className="my-5">
        Then, Create a new git repository and add your remote.
      </p>
      <Copycode language="bash">git remote add dev &quot;remote&quot;</Copycode>
      <h2 id="setup-tools">Setup tools</h2>
      <ul className="list-disc list-inside">
        <li>
          <a
            href="https://github.com/johnnymorganz/stylua#installation"
            target="_blank"
          >
            <b>Stylua</b>
          </a>{" "}
          for formatting.
        </li>
        <li>
          <a href="https://github.com/luarocks/luacheck" target="_blank">
            <b>Luacheck</b>
          </a>{" "}
          for linting.
        </li>
      </ul>
      <p className="my-5">To run stylua follow this command.</p>
      <Copycode language="bash">make style</Copycode>
      <p className="my-5">To run luacheck follow this command.</p>
      <Copycode language="bash">make lint</Copycode>
      <h2 id="commit-messages">Commit Messages</h2>
      <p className="mb-5">
        <b>Note :</b> Commit header is limited to 72 characters.
      </p>
      <Codeblock language="text" showLineNumbers={false}>
        {`<type>(<scope>?): <summary>
  │       │           │
  │       │           └─> Present tense.     'add something...'(O) vs 'added something...'(X)
  │       │               Imperative mood.   'move cursor to...'(O) vs 'moves cursor to...'(X)
  │       │               Not capitalized.
  │       │               No period at the end.
  │       │
  │       └─> Commit Scope is optional, but strongly recommended.
  │           Use lower case.
  │           'plugin', 'file', or 'directory' name is suggested, but not limited.
  │
  └─> Commit Type: build|ci|docs|feat|fix|perf|refactor|test`}
      </Codeblock>
      <ul className="list-disc mt-5 list-inside">
        <li className="mb-2">
          <b>build :</b> changes that affect the build system or external
          dependencies (example scopes: npm, pip, rg)
        </li>
        <li className="mb-2">
          <b>ci :</b> changes to CI configuration files and scripts (example
          scopes: format, lint, issue_templates)
        </li>
        <li className="mb-2">
          <b>docs :</b> changes to the documentation only
        </li>
        <li className="mb-2">
          <b>feat :</b> new feature for the user
        </li>
        <li className="mb-2">
          <b>fix :</b> bug fix
        </li>
        <li className="mb-2">
          <b>perf :</b> performance improvement
        </li>
        <li className="mb-2">
          <b>refactor :</b> code change that neither fixes a bug nor adds a
          feature
        </li>
        <li className="mb-2">
          <b>test :</b> adding missing tests or correcting existing tests
        </li>
        <li>
          <b>chore :</b> all the rest, including version bump for plugins
        </li>
      </ul>
      <h2 id="communication">Communication</h2>
      <ul className="list-disc list-inside">
        <li>
          <a href="https://discord.gg/uF2SkwAw" target="_blank">
            Discord server
          </a>
        </li>
      </ul>
    </div>
  )
}

export default page
