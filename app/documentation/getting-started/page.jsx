import { Codetag, Copycode } from "../../../components/code"

const page = () => {
  return (
    <div className="flex flex-col w-full h-fit">
      <h1 id="getting-started">Getting Started</h1>
      <p>
        DomacsVim - A free, open source and intelligent IDE for NeoVim. An IDE
        layer for Neovim with beautiful, flexible, extensible and completely
        free and open source lua-based configurations for Unix-based systems.
      </p>
      <h2 id="prerequisites">Prerequisites</h2>
      <p>
        In the first step, you must make sure that the desired prerequisites and
        dependencies are installed on your system.
      </p>
      <ul className="list-disc list-inside my-5">
        <li>
          Make sure you have installed the latest version of{" "}
          <a href="https://github.com/neovim/neovim/releases">Neovim v0.9.0+</a>
          .
        </li>
        <li>
          Have python, pip, npm, node, yarn and cargo (for lsp and dap servers)
          installed on your system.
        </li>
        <li>
          Install <a href="https://www.nerdfonts.com/">Nerd Font</a> on your
          terminal. (for icons)
        </li>
        <li>
          <a href="https://github.com/BurntSushi/ripgrep">Ripgrep</a> for
          searching words on your project. (for telescope)
        </li>
      </ul>
      <p>
        <b>but do not worry !</b> You can install these dependencies while
        running the installer.
      </p>
      <h2 id="installation">Installation</h2>
      <p>
        You can start the installation process by writing a command line. You
        will be asked several questions to install the desired packages. and if
        needed (the package in question is not pre-installed on your system) you
        answer with ( y ) and otherwise (if the package in question is
        pre-installed on your system) with ( n ) you answer and that&aposs it.
      </p>
      <h4>
        Install the rooling Version{" "}
        <span className="text-white text-opacity-40">(neovim v0.10.0+)</span>
      </h4>
      <Copycode language="bash">
        {
          "sh <(curl -s 'https://gitlab.com/domacsvim/domacsvim/-/raw/main/installer/install.sh')"
        }
      </Copycode>
      <h4>
        Install the latest Version{" "}
        <span className="text-white text-opacity-40">(neovim v0.9.0)</span>
      </h4>
      <Copycode language="bash">
        {
          "sh <(curl -s 'https://gitlab.com/domacsvim/domacsvim/-/raw/V-1.0.0/installer/install.sh')"
        }
      </Copycode>
      <h2 id="try-it-with-docker">Try it with Docker</h2>
      <p>
        You can do it with the purpose of testing or installing DomacsVim on a
        completely isolated environment (Docker) and only by running the
        following command.
      </p>
      <div className="mt-5">
        <Copycode language="bash" showLineNumbers={false} wrap={true}>
          {`docker run -w /root -it --rm alpine:latest sh -uelic "\\
  apk add git nodejs neovim ripgrep build-base wget curl --update && \\
  sh <(curl -s 'https://gitlab.com/domacsvim/domacsvim/-/raw/V-1.0.0/installer/install.sh') && \\
  dvim"`}
        </Copycode>
      </div>
      <h2 id="update">Update</h2>
      <p>To update DomacsVim run the following command :</p>
      <div className="flex mt-5">
        <Codetag language="bash">:DvimUpdate</Codetag>
      </div>
      <p className="mt-5">To update Plugins run the following command :</p>
      <div className="flex mt-5">
        <Codetag language="bash">:Lazy update</Codetag>
      </div>
      <h2 id="uninstallation">Uninstallation</h2>
      <div className="flex flex-col mt-5">
        <Copycode language="bash">
          {"sh '~/.local/share/domacsvim/installer/uninstall.sh'"}
        </Copycode>
        <span className="my-2">or</span>
        <Copycode language="bash">
          {
            "sh <(curl -s 'https://gitlab.com/domacsvim/domacsvim/-/raw/main/installer/uninstall.sh')"
          }
        </Copycode>
      </div>
    </div>
  )
}

export default page
