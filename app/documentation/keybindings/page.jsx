const page = () => {
  return (
    <div className="flex flex-col w-full">
      <h1 id="keybindings">Keybindings</h1>
      <h2 id="general">General</h2>
      <table class="min-w-full text-left">
        <thead class="border-b border-white border-opacity-40 bg-white bg-opacity-25">
          <tr className="rounded-xl">
            <th scope="col" class="px-6 py-4">
              Key
            </th>
            <th scope="col" class="px-6 py-4">
              Description
            </th>
            <th scope="col" class="px-6 py-4">
              Mode
            </th>
          </tr>
        </thead>
        <tbody>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`<A-k>`}</td>
            <td class="whitespace-nowrap px-6 py-4">Move this line to up</td>
            <td class="whitespace-nowrap px-6 py-4">insert</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`<A-j>`}</td>
            <td class="whitespace-nowrap px-6 py-4">Move this line to down</td>
            <td class="whitespace-nowrap px-6 py-4">insert</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`<C-h>`}</td>
            <td class="whitespace-nowrap px-6 py-4">Left arrow</td>
            <td class="whitespace-nowrap px-6 py-4">insert</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`<C-l>`}</td>
            <td class="whitespace-nowrap px-6 py-4">Right arrow</td>
            <td class="whitespace-nowrap px-6 py-4">insert</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`<C-j>`}</td>
            <td class="whitespace-nowrap px-6 py-4">Down arrow</td>
            <td class="whitespace-nowrap px-6 py-4">insert</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`<C-k>`}</td>
            <td class="whitespace-nowrap px-6 py-4">Up arrow</td>
            <td class="whitespace-nowrap px-6 py-4">insert</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`<ESC>`}</td>
            <td class="whitespace-nowrap px-6 py-4">Clear search highlights</td>
            <td class="whitespace-nowrap px-6 py-4">normal</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`<C-n>`}</td>
            <td class="whitespace-nowrap px-6 py-4">New file</td>
            <td class="whitespace-nowrap px-6 py-4">normal</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`<C-u>`}</td>
            <td class="whitespace-nowrap px-6 py-4">Undo</td>
            <td class="whitespace-nowrap px-6 py-4">normal</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`<C-r>`}</td>
            <td class="whitespace-nowrap px-6 py-4">Redo</td>
            <td class="whitespace-nowrap px-6 py-4">normal</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`<C-a>`}</td>
            <td class="whitespace-nowrap px-6 py-4">Select all</td>
            <td class="whitespace-nowrap px-6 py-4">insert</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`<C-y>`}</td>
            <td class="whitespace-nowrap px-6 py-4">Copy this line</td>
            <td class="whitespace-nowrap px-6 py-4">normal</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`<C-x>`}</td>
            <td class="whitespace-nowrap px-6 py-4">Delete this line</td>
            <td class="whitespace-nowrap px-6 py-4">normal</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`<C-h>`}</td>
            <td class="whitespace-nowrap px-6 py-4">Change buffer to left</td>
            <td class="whitespace-nowrap px-6 py-4">normal</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`<C-j>`}</td>
            <td class="whitespace-nowrap px-6 py-4">Change buffer to right</td>
            <td class="whitespace-nowrap px-6 py-4">normal</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`<C-k>`}</td>
            <td class="whitespace-nowrap px-6 py-4">Move buffer to up</td>
            <td class="whitespace-nowrap px-6 py-4">normal</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`<C-l>`}</td>
            <td class="whitespace-nowrap px-6 py-4">Move buffer to up</td>
            <td class="whitespace-nowrap px-6 py-4">normal</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`<C-up>`}</td>
            <td class="whitespace-nowrap px-6 py-4">Resize buffer height</td>
            <td class="whitespace-nowrap px-6 py-4">normal</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`<C-down>`}</td>
            <td class="whitespace-nowrap px-6 py-4">Resize buffer height</td>
            <td class="whitespace-nowrap px-6 py-4">normal</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`<C-left>`}</td>
            <td class="whitespace-nowrap px-6 py-4">Resize buffer width</td>
            <td class="whitespace-nowrap px-6 py-4">normal</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`<C-right>`}</td>
            <td class="whitespace-nowrap px-6 py-4">Resize buffer width</td>
            <td class="whitespace-nowrap px-6 py-4">normal</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`<A-k>`}</td>
            <td class="whitespace-nowrap px-6 py-4">Resize buffer height</td>
            <td class="whitespace-nowrap px-6 py-4">normal</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`<A-j>`}</td>
            <td class="whitespace-nowrap px-6 py-4">Resize buffer height</td>
            <td class="whitespace-nowrap px-6 py-4">normal</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`<A-h>`}</td>
            <td class="whitespace-nowrap px-6 py-4">Resize buffer width</td>
            <td class="whitespace-nowrap px-6 py-4">normal</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`<A-l>`}</td>
            <td class="whitespace-nowrap px-6 py-4">Resize buffer width</td>
            <td class="whitespace-nowrap px-6 py-4">normal</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`f`}</td>
            <td class="whitespace-nowrap px-6 py-4">Code folding</td>
            <td class="whitespace-nowrap px-6 py-4">normal</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`E`}</td>
            <td class="whitespace-nowrap px-6 py-4">End of line</td>
            <td class="whitespace-nowrap px-6 py-4">normal</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`H`}</td>
            <td class="whitespace-nowrap px-6 py-4">Beginning of line</td>
            <td class="whitespace-nowrap px-6 py-4">insert</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`>`}</td>
            <td class="whitespace-nowrap px-6 py-4">
              Tab indent (by default 2 space)
            </td>
            <td class="whitespace-nowrap px-6 py-4">normal</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`<`}</td>
            <td class="whitespace-nowrap px-6 py-4">Back indent</td>
            <td class="whitespace-nowrap px-6 py-4">normal</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`>`}</td>
            <td class="whitespace-nowrap px-6 py-4">Tab indent</td>
            <td class="whitespace-nowrap px-6 py-4">visual</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`<`}</td>
            <td class="whitespace-nowrap px-6 py-4">Back indent</td>
            <td class="whitespace-nowrap px-6 py-4">visual</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`H`}</td>
            <td class="whitespace-nowrap px-6 py-4">Beginning of line</td>
            <td class="whitespace-nowrap px-6 py-4">visual</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`E`}</td>
            <td class="whitespace-nowrap px-6 py-4">End of line</td>
            <td class="whitespace-nowrap px-6 py-4">visual</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`<A-j>`}</td>
            <td class="whitespace-nowrap px-6 py-4">Move line to down</td>
            <td class="whitespace-nowrap px-6 py-4">visual</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`<A-k>`}</td>
            <td class="whitespace-nowrap px-6 py-4">Move line to up</td>
            <td class="whitespace-nowrap px-6 py-4">visual</td>
          </tr>
        </tbody>
      </table>
      <h2 id="nvimtree">Nvimtree</h2>
      <table class="min-w-full text-left">
        <thead class="border-b border-white border-opacity-40 bg-white bg-opacity-25">
          <tr className="rounded-xl">
            <th scope="col" class="px-6 py-4">
              Key
            </th>
            <th scope="col" class="px-6 py-4">
              Description
            </th>
            <th scope="col" class="px-6 py-4">
              Mode
            </th>
          </tr>
        </thead>
        <tbody>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`<C-e>`}</td>
            <td class="whitespace-nowrap px-6 py-4">Open file explorer</td>
            <td class="whitespace-nowrap px-6 py-4">normal</td>
          </tr>
        </tbody>
      </table>
      <h2 id="gitsigns">Gitsigns</h2>
      <table class="min-w-full text-left">
        <thead class="border-b border-white border-opacity-40 bg-white bg-opacity-25">
          <tr className="rounded-xl">
            <th scope="col" class="px-6 py-4">
              Key
            </th>
            <th scope="col" class="px-6 py-4">
              Description
            </th>
            <th scope="col" class="px-6 py-4">
              Mode
            </th>
          </tr>
        </thead>
        <tbody>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`gh`}</td>
            <td class="whitespace-nowrap px-6 py-4">Reset Git hunk</td>
            <td class="whitespace-nowrap px-6 py-4">normal</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`gn`}</td>
            <td class="whitespace-nowrap px-6 py-4">Git next hunk</td>
            <td class="whitespace-nowrap px-6 py-4">normal</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`gp`}</td>
            <td class="whitespace-nowrap px-6 py-4">Git previos hunk</td>
            <td class="whitespace-nowrap px-6 py-4">normal</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`gk`}</td>
            <td class="whitespace-nowrap px-6 py-4">Git preview hunk</td>
            <td class="whitespace-nowrap px-6 py-4">normal</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`gi`}</td>
            <td class="whitespace-nowrap px-6 py-4">Git preview inline hunk</td>
            <td class="whitespace-nowrap px-6 py-4">normal</td>
          </tr>
        </tbody>
      </table>
      <h2 id="comments">Comments</h2>
      <table class="min-w-full text-left">
        <thead class="border-b border-white border-opacity-40 bg-white bg-opacity-25">
          <tr className="rounded-xl">
            <th scope="col" class="px-6 py-4">
              Key
            </th>
            <th scope="col" class="px-6 py-4">
              Description
            </th>
            <th scope="col" class="px-6 py-4">
              Mode
            </th>
          </tr>
        </thead>
        <tbody>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`<C-/>`}</td>
            <td class="whitespace-nowrap px-6 py-4">Toggle comment</td>
            <td class="whitespace-nowrap px-6 py-4">normal</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`<C-/>`}</td>
            <td class="whitespace-nowrap px-6 py-4">Toggle comment</td>
            <td class="whitespace-nowrap px-6 py-4">visual</td>
          </tr>
        </tbody>
      </table>
      <h2 id="terminal">Terminal</h2>
      <table class="min-w-full text-left">
        <thead class="border-b border-white border-opacity-40 bg-white bg-opacity-25">
          <tr className="rounded-xl">
            <th scope="col" class="px-6 py-4">
              Key
            </th>
            <th scope="col" class="px-6 py-4">
              Description
            </th>
            <th scope="col" class="px-6 py-4">
              Mode
            </th>
          </tr>
        </thead>
        <tbody>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`t`}</td>
            <td class="whitespace-nowrap px-6 py-4">Toggle term mode</td>
            <td class="whitespace-nowrap px-6 py-4">normal</td>
          </tr>
        </tbody>
      </table>
      <h2 id="bufferline">Bufferline</h2>
      <table class="min-w-full text-left">
        <thead class="border-b border-white border-opacity-40 bg-white bg-opacity-25">
          <tr className="rounded-xl">
            <th scope="col" class="px-6 py-4">
              Key
            </th>
            <th scope="col" class="px-6 py-4">
              Description
            </th>
            <th scope="col" class="px-6 py-4">
              Mode
            </th>
          </tr>
        </thead>
        <tbody>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`<C-tab>`}</td>
            <td class="whitespace-nowrap px-6 py-4">Switch to next buffer</td>
            <td class="whitespace-nowrap px-6 py-4">normal</td>
          </tr>
          <tr class="border-b border-white border-opacity-40 bg-white bg-opacity-20">
            <td class="whitespace-nowrap px-6 py-4">{`<S-tab>`}</td>
            <td class="whitespace-nowrap px-6 py-4">Switch to pre buffer</td>
            <td class="whitespace-nowrap px-6 py-4">normal</td>
          </tr>
        </tbody>
      </table>
    </div>
  )
}

export default page
