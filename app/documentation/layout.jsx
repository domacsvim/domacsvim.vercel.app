"use client"
import Link from "next/link"
import Config from "../../domacsvim.config"
import { usePathname } from "next/navigation"
import { useState } from "react"

const Layout = ({ children }) => {
  const currentPage = usePathname()
  const [is_toggle_menu_icon, set_toggle_menu_icon] = useState(false)
  return (
    <>
      <title>DomacsVim | Documentation</title>
      <div className="flex w-full bg-black">
        <div className="flex w-full text-lg sticky top-10">
          <div className="lg:flex lg:flex-col hidden w-2/12 pt-10">
            <ul className="mx-5 sticky top-10">
              {Config.documentation.sidebar.pages.map((page, idx) => {
                if (currentPage == page.url) {
                  return (
                    <Link href={page.url} key={idx}>
                      <li className="bg-white bg-opacity-20 rounded-md py-1 px-4 my-2">
                        {page.title}
                      </li>
                    </Link>
                  )
                } else {
                  return (
                    <Link href={page.url} key={idx}>
                      <li className="py-1 px-4 my-2">{page.title}</li>
                    </Link>
                  )
                }
              })}
            </ul>
          </div>
          <div className="flex flex-col py-10 lg:px-20 lg:w-8/12 w-full">
            <div class="flex mt-2 w-full px-10 lg:hidden">
              <div className="flex flex-col w-full">
                <ul className="text-black bg-white rounded-t-lg">
                  {Config.documentation.sidebar.pages.map((page, idx) => {
                    if (currentPage == page.url) {
                      return (
                        <Link
                          href={page.url}
                          key={idx}
                          className="flex w-full items-center"
                          onClick={() =>
                            set_toggle_menu_icon(!is_toggle_menu_icon)
                          }
                        >
                          <li className="bg-white bg-opacity-20 rounded-md py-1 px-4 mr-auto">
                            {page.title}
                          </li>
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            strokeWidth={1.5}
                            stroke="currentColor"
                            className={`h-6 w-6 mr-2 ${
                              is_toggle_menu_icon ? "hidden" : "block"
                            }`}
                          >
                            <path
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              d="M15.75 19.5L8.25 12l7.5-7.5"
                            />
                          </svg>
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            strokeWidth={1.5}
                            stroke="currentColor"
                            className={`h-6 w-6 mr-4 ${
                              is_toggle_menu_icon ? "block" : "hidden"
                            }`}
                          >
                            <path
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              d="M19.5 8.25l-7.5 7.5-7.5-7.5"
                            />
                          </svg>
                        </Link>
                      )
                    }
                  })}
                </ul>
                <ul
                  className={`bg-white w-full mt-1 rounded-b-lg flex flex-col md:hidden text-black ${
                    is_toggle_menu_icon ? "block" : "hidden"
                  }`}
                >
                  {Config.documentation.sidebar.pages.map((page, idx) => {
                    if (currentPage != page.url) {
                      return (
                        <Link href={page.url} key={idx}>
                          <li className="py-1 px-4 my-2">{page.title}</li>
                        </Link>
                      )
                    }
                  })}
                </ul>
              </div>
            </div>
            <div className="lg:flex hidden lg:mb-10 mb-5">
              <Link href="/">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth={1.5}
                  stroke="currentColor"
                  className="w-6 h-6"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M2.25 12l8.954-8.955c.44-.439 1.152-.439 1.591 0L21.75 12M4.5 9.75v10.125c0 .621.504 1.125 1.125 1.125H9.75v-4.875c0-.621.504-1.125 1.125-1.125h2.25c.621 0 1.125.504 1.125 1.125V21h4.125c.621 0 1.125-.504 1.125-1.125V9.75M8.25 21h8.25"
                  />
                </svg>
              </Link>
              <pre> </pre>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className="w-4"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M8.25 4.5l7.5 7.5-7.5 7.5"
                />
              </svg>
              <pre> </pre>
              <Link href="/documentation">Documentation</Link>
              {Config.documentation.sidebar.pages.map((page, idx) => {
                if (currentPage === page.url) {
                  return (
                    <div className="flex" key={idx}>
                      <pre> </pre>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth={1.5}
                        stroke="currentColor"
                        className="w-4"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M8.25 4.5l7.5 7.5-7.5 7.5"
                        />
                      </svg>
                      <pre> </pre>
                      <Link href={page.url}>{page.title}</Link>
                    </div>
                  )
                }
              })}
            </div>
            <div id="Document" className="flex w-full px-10">
              {children}
            </div>
          </div>
          <div className="lg:flex lg:flex-col hidden pt-10 w-2/12 ml-auto">
            <ul className="mx-5 border-l-2 border-blue-700 sticky top-10">
              {Config.documentation.sidebar.pages.map((page) => {
                if (currentPage === page.url) {
                  return page.blocks.map((block, idx) => {
                    return (
                      <Link href={block.href} key={idx}>
                        <li className="py-1 px-4 my-2">{block.id}</li>
                      </Link>
                    )
                  })
                }
              })}
            </ul>
          </div>
        </div>
      </div>
    </>
  )
}

export default Layout
