import "../styles/globals.css"
import { Ubuntu } from "next/font/google"
import Header from "../components/header"
import Footer from "../components/footer"

const ubuntu = Ubuntu({
  weight: "400",
  subsets: ["latin"],
  display: "swap",
})

const Layout = ({ children }) => {
  return (
    <html lang="en">
      <head>
        <title>DomacsVim</title>
        <link rel="icon" href="/favicon.png" type="image/x-icon" sizes="any" />
        <meta name="description" content="DomacsVim official website." />
      </head>
      <body className={`flex flex-col w-full ${ubuntu.className}`}>
        <Header />
        {children}
        <Footer />
      </body>
    </html>
  )
}

export default Layout
