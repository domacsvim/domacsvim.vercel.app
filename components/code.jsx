"use client"
import { Code, CopyBlock, CodeBlock, atomOneDark } from "react-code-blocks"

const Codetag = ({ children, language, showLineNumbers }) => {
  return (
    <Code
      text={children}
      language={language}
      showLineNumbers={showLineNumbers}
      theme={atomOneDark}
      customStyle={{
        borderRadius: "5px",
        padding: "5px",
      }}
    />
  )
}

const Codeblock = ({ children, language, showLineNumbers }) => {
  return (
    <div className="font-mono text-lg select-text pb-0 w-full">
      <CodeBlock
        text={children}
        language={language}
        showLineNumbers={showLineNumbers}
        theme={atomOneDark}
        customStyle={{
          display: "flex",
          borderRadius: "5px",
          padding: "10px",
        }}
        wrapLines
      />
    </div>
  )
}

const Copycode = ({ children, language, showLineNumbers, wrap }) => {
  return (
    <div className="font-mono text-lg select-text pb-0 w-full">
      <CopyBlock
        text={children}
        language={language}
        showLineNumbers={showLineNumbers}
        theme={atomOneDark}
        customStyle={{
          display: "flex",
          borderRadius: "5px",
          padding: "10px",
        }}
        wrapLongLines={wrap}
        wrapLines
      />
    </div>
  )
}

export { Codetag, Codeblock, Copycode }
