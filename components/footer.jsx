const Footer = () => {
  return (
    <footer className="flex flex-col w-full bg-black">
      <div className="w-full h-0.5 bg-gradient-to-r from-blue-700"></div>
      <div className="flex lg:flex-row flex-col justify-center w-full container mx-auto items-center text-lg m-5">
        <span className="lg:hidden lg:mr-0 mr-10 text-lg">Copyright &copy; 2023</span>
        <div className="flex w-auto items-center">
          <span className="mr-10 lg:inline hidden">Copyright &copy; 2023</span>
          <a
            href="https://discord.gg/uF2SkwAw"
            className="hover:underline underline-offset-8 py-5 px-2.5 flex items-center"
            target="_blank"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={1.5}
              stroke="currentColor"
              className="w-6 h-6 mr-2"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M20.25 8.511c.884.284 1.5 1.128 1.5 2.097v4.286c0 1.136-.847 2.1-1.98 2.193-.34.027-.68.052-1.02.072v3.091l-3-3c-1.354 0-2.694-.055-4.02-.163a2.115 2.115 0 01-.825-.242m9.345-8.334a2.126 2.126 0 00-.476-.095 48.64 48.64 0 00-8.048 0c-1.131.094-1.976 1.057-1.976 2.192v4.286c0 .837.46 1.58 1.155 1.951m9.345-8.334V6.637c0-1.621-1.152-3.026-2.76-3.235A48.455 48.455 0 0011.25 3c-2.115 0-4.198.137-6.24.402-1.608.209-2.76 1.614-2.76 3.235v6.226c0 1.621 1.152 3.026 2.76 3.235.577.075 1.157.14 1.74.194V21l4.155-4.155"
              />
            </svg>
            Community
          </a>
          <a
            href="https://gitlab.com/domacsvim/domacsvim"
            className="hover:underline underline-offset-8 py-5 px-2.5 flex items-center"
            target="_blank"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={1.5}
              stroke="currentColor"
              className="w-6 h-6 mr-2"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M17.25 6.75L22.5 12l-5.25 5.25m-10.5 0L1.5 12l5.25-5.25m7.5-3l-4.5 16.5"
              />
            </svg>
            Source Code
          </a>
        </div>
        <div className="flex lg:ml-auto items-center">
          <a
            href="https://discord.gg/uF2SkwAw"
            className="hover:underline underline-offset-8 py-5 px-2.5 flex items-center"
            target="_blank"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={1.5}
              stroke="currentColor"
              className="w-6 h-6 mr-2"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M15.75 5.25a3 3 0 013 3m3 0a6 6 0 01-7.029 5.912c-.563-.097-1.159.026-1.563.43L10.5 17.25H8.25v2.25H6v2.25H2.25v-2.818c0-.597.237-1.17.659-1.591l6.499-6.499c.404-.404.527-1 .43-1.563A6 6 0 1121.75 8.25z"
              />
            </svg>
            License
          </a>
          <span className="ml-8">
            Developed by{" "}
            <span className="text-purple font-bold">DomacsVim</span>
          </span>
        </div>
      </div>
    </footer>
  )
}

export default Footer
