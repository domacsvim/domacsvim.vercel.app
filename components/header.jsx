"use client"
import Link from "next/link"
import { useState } from "react"

const Header = () => {
  const [is_toggle_menu_icon, set_toggle_menu_icon] = useState(false)

  return (
    <header className="flex flex-col w-full bg-black">
      <div className="flex lg:container lg:mx-auto mx-10 items-center lg:py-0 py-3">
        <Link href="/">
          <h2 className="text-whtie text-2xl">DomacsVim</h2>
        </Link>
        <div className="hidden md:flex">
          <Link
            href="/documentation/getting-started"
            className="text-white hover:underline ml-10 py-4"
          >
            Documentation
          </Link>
        </div>
        <div className="flex md:hidden ml-auto">
          <button
            onClick={() => set_toggle_menu_icon(!is_toggle_menu_icon)}
            className="flex items-center px-3 py-2 rounded text-white"
          >
            <svg
              className={`fill-current h-5 w-5 ${
                is_toggle_menu_icon ? "hidden" : "block"
              }`}
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
            </svg>
            <svg
              className={`fill-current h-5 w-5 ${
                is_toggle_menu_icon ? "block" : "hidden"
              }`}
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path d="M10 8.586L2.929 1.515 1.515 2.929 8.586 10l-7.071 7.071 1.414 1.414L10 11.414l7.071 7.071 1.414-1.414L11.414 10l7.071-7.071-1.414-1.414L10 8.586z" />
            </svg>
          </button>
        </div>
        <div className="hidden w-full md:flex">
          <button className="border-white border-2 ml-auto py-2 px-5 rounded-full hover:bg-white hover:bg-opacity-20">
            <Link href="/documentation/getting-started">V 1.0.0</Link>
          </button>
          <button className="bg-blue-700 ml-5 py-2 px-5 rounded-full hover:bg-blue-600">
            <Link href="/documentation/getting-started">Get Started</Link>
          </button>
        </div>
      </div>
      <div
        className={`flex flex-col mx-10 mb-5 md:hidden ${
          is_toggle_menu_icon ? "block" : "hidden"
        }`}
      >
        <div className="flex flex-col text-lg">
          <Link
            href="/documentation/getting-started"
            className="text-white hover:underline py-4"
          >
            Documentation
          </Link>
          <button className="border-white border-2 mt-5 py-2 px-5 rounded-full hover:bg-white hover:bg-opacity-20">
            <Link href="/documentation/getting-started">V 1.0.0</Link>
          </button>
          <button className="bg-blue-700 py-2 px-5 mt-5 rounded-full hover:bg-blue-600">
            <Link href="/documentation/getting-started">Get Started</Link>
          </button>
        </div>
      </div>
      <div className="w-full h-0.5 bg-gradient-to-r from-blue-700"></div>
    </header>
  )
}

export default Header
