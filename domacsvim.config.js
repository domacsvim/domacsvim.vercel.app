const defaults = {
  documentation: {
    sidebar: {
      pages: [
        {
          title: "Getting Started",
          url: "/documentation/getting-started",
          blocks: [
            { id: "Getting Started", href: "#getting-started" },
            { id: "Prerequisites", href: "#prerequisites" },
            { id: "Installation", href: "#installation" },
            { id: "Try it with docker", href: "#try-it-with-docker" },
            { id: "Update", href: "#update" },
            { id: "Uninstallation", href: "#uninstallation" },
          ],
        },
        {
          title: "Beginner's Guide",
          url: "/documentation/beginners-guide",
          blocks: [
            { id: "Beginner's Guide", href: "#beginners-guide" },
            { id: "Learn Lua Language", href: "#learn-lua-lang" },
          ],
        },
        {
          title: "Configuration",
          url: "/documentation/configuration/overview",
          blocks: [
            { id: "Configuration", href: "#configuration" },
            { id: "keybindings", href: "#keybindings" },
            { id: "Mapping format", href: "#mapping-format" },
            { id: "Whichkey", href: "#whichkey" },
            { id: "Whichkey format", href: "#whichkey-format" },
            { id: "Plugins", href: "#plugins" },
            {
              id: "Configure builtin plugins",
              href: "#configure-builtin-plugins",
            },
            { id: "Colorschemes", href: "#colorschemes" },
            { id: "Default icons", href: "#default-icons" },
            { id: "Options", href: "#options" },
          ],
        },
        {
          title: "Keybindings",
          url: "/documentation/keybindings",
          blocks: [
            { id: "Keybindings", href: "#keybindigns" },
            { id: "General", href: "#general" },
            { id: "Nvimtree", href: "#nvimtree" },
            { id: "Gitsigns", href: "#gitsigns" },
            { id: "Comments", href: "#comments" },
            { id: "Terminal", href: "#terminal" },
            { id: "Bufferline", href: "#bufferline" },
          ],
        },
        {
          title: "Development",
          url: "/documentation/development",
          blocks: [
            { id: "Development", href: "#development" },
            { id: "Structure", href: "#structure" },
            { id: "Get started", href: "#get-started" },
            { id: "Setup tools", href: "#setup-tools" },
            { id: "Commit messages", href: "#commit-messages" },
            { id: "Communication", href: "#communication" },
          ],
        },
      ],
    },
  },
}

module.exports = defaults
