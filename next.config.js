const defaults = {
  reactStrictMode: true,
  swcMinify: true,
  async redirects() {
    return [
      {
        source: "/documentation",
        destination: "/documentation/getting-started",
        permanent: true,
      },
      {
        source: "/documentation/configuration",
        destination: "/documentation/configuration/overview",
        permanent: true,
      },
    ]
  },
}

module.exports = defaults
