const defaults = {
  plugins: {
    tailwindcss: {},
    autoprefixer: {},
  },
}

module.exports = defaults
